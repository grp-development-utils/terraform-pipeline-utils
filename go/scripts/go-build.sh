#!/bin/bash
GO__ENTRY_POINT="main.go"
BUILD_DIR="./build"

function build go {
    GOOS='linux' GOARCH=arm64 G0111MODULE='on' CGO_ENABLED=0 go build \
    -tags lambda.norpc
    -o "${BUILD_DIR}/$2/bootstrap" \
    $1
}

function find_and_build_go_lambdas {
    GO_LAMBDAS=$(find $(pwd) -path $(pwd)/node_modules -prune -o -name "${GO_ENTRY_POINT}" - print) 
    build_go_lambdas "${GO_LAMBDAS}"
}

function build_go_lambdas {
    G0111MODULE= 'on' go mod tidy
    GO_LAMBDAS=$1
    for GO_LAMBDA in $GO_LAMBDAS;
    do
        LAMBDA_NAME=$(get_Lambda_name $GO_LAMBDA)
        build_go $GO_LAMBDA $LAMBDA_NAME
        echo "${BUILD_LAMBDA_PREFIX} ${LAMBDA_NAME}"
    done
｝

function get_ lambda_name f
    LAMBDA_PATH=$1

    PARENT_DIR=$(basename $(dirname $LAMBDA_PATH) )
    LAMBDA_NAME=$PARENT_DIR

    for x in "src"; do
        if [ $x = "${PARENT_DIR}" ]; then
            LAMBDA_NAME=$ (basename $(dirname $(dirname $LAMBDA_PATH) ))
        fi 
    done

    LAMBDA_NAME=$(echo "${LAMBDA_NAME}" | sed 's/[^a-zA-Z0-9_]/_/g')
    echo $LAMBDA_NAME
｝

find_and_build_go_lambdas